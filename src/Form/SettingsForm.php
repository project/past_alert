<?php

namespace Drupal\past_alert\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return [
    'past_alert.settings',
    ];
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'past_alert_settings';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('past_alert.settings');

    $form['email_to'] = [
    '#type' => 'textfield',
    '#title' => $this->t('Send alert to:'),
    '#default_value' => $config->get('email_to'),
    ];

    $included = [];
    $levels = [];

    foreach (RfcLogLevel::getLevels() as $key => $value) {
      if (in_array($key, $config->get('severities'))) {
        $included[$key] = $key;
      }
      $levels[$key] = $value;
    }

    $form['severities'] = [
    '#type' => 'checkboxes',
    '#default_value' => $included,
    '#options' => $levels,
    '#title' => t('Severity levels'),
    '#description' => t('Please select severity levels you want to receive alerts for'),
    ];

    $form['mail_body'] = [
      '#type' => 'radios',
      '#title' => t('What to include in the mail body?'),
      '#default_value' => $config->get('mail_body'),
      '#options' => [
        '' => t('Nothing'),
        'exception' => t('Only exceptions'),
        'all' => t('All Past arguments'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('past_alert.settings')
      ->set('email_to', $form_state->getValue('email_to'))
      ->set('severities', $form_state->getValue('severities'))
      ->set('mail_body', $form_state->getValue('mail_body'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
