<?php

namespace Drupal\past_alert\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Class EmailManager.
 */
class EmailManager {

  /**
   * @inheritdoc
   */
  protected $config;

  /**
   * @inheritdoc
   */
  protected $currentUser;

  /**
   * @inheritdoc
   */
  protected $emailManager;

  /**
   * EmailManager constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountProxyInterface $current_user, MailManagerInterface $email_manager) {
    $this->config = $config_factory->get('past_alert.settings');
    $this->currentUser = $current_user;
    $this->emailManager = $email_manager;
  }

  /**
   * @inheritdoc
   */
  public function process($entity) {
    $config = $this->config;

    // Check severity.
    if (empty($config->get('severities')[$entity->getSeverity()])) {
      return;
    }

    // If email_to is empty - skip it.
    if (empty($config->get('email_to'))) {
      return;
    }

    // Everything is good, send an email.
    $this->prepareSendMail($entity);
  }

  /**
   * @inheritdoc
   */
  public function prepareSendMail($entity) {
    $module = 'past_alert';
    $key = 'past_alert_mail';
    $to = $this->config->get('email_to');
    $langcode = $this->currentUser->getPreferredLangcode();
    $severity = $this->getSeverityName($entity->getSeverity());

    $message[] = 'Module: ' . $entity->getModule();
    $message[] = 'Severity: ' . $severity;
    $message[] = 'Message: ' . $entity->getMessage();
    $message[] = 'Link: ' . $entity->toUrl('canonical', ['absolute' => TRUE])->toString();
    $message[] = '';

    if (!empty($this->config->get('mail_body'))) {
      $mail_body = $this->config->get('mail_body');
      foreach ($entity->getArguments() as $argument) {
        $data = !empty($argument->getOriginalData()) ? $argument->getOriginalData() : $argument->getData();
        if (is_array($data)) {
          $data = json_encode($data);
        }

        if ($mail_body === 'exception' && $argument->getKey() === 'exception') {
          $add_to_body = TRUE;
        }
        elseif ($mail_body === 'all') {
          $add_to_body = TRUE;
        }
        else {
          $add_to_body = FALSE;
        }

        if ($add_to_body) {
          $message[] = 'Argument "' . $argument->defaultLabel() . '":';
          $message[] = $data;
          $message[] = '';
        }

      }
    }

    $params = [
      'subject' => 'New Past Log entry with severity: ' . strtoupper($severity),
      'body' => implode("\r\n", $message),
    ];

    $this->emailManager->mail($module, $key, $to, $langcode, $params, FALSE, TRUE);
  }

  /**
   * @inheritDoc
   */
  public function getSeverityName($severity) {
    $severities = RfcLogLevel::getLevels();
    if (isset($severities[$severity])) {
      return $severities[$severity];
    }
    return $severity;
  }

}
