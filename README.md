<pre>
  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘
</pre>

CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Configuration


INTRODUCTION
------------

This module provides simple E-mail alerts for the Past Log module.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

* Past-alert configuration is located here: Configuration > Development > Past > Past Alert
  (/admin/config/development/past/past-alert)
* Fill in your email address and select severity levels you want to receive alerts for
* Once the new past log is added - you should have it in your e-mail inbox


by acolono GmbH
---------------

~~we build your websites~~
we build your business

hello@acolono.com

www.acolono.com
www.twitter.com/acolono
www.drupal.org/acolono-gmbh
